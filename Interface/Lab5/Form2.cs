﻿using Lab5;
using MusicLib;
using MusicLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MusicDesktop
{
    public partial class AddForm : Form
    {
        private AbstractMusic _music;
        public AbstractMusic Music
        {
            get { return _music; }
            set { _music = value; }
        }
        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        public AddForm()
        {
            InitializeComponent();
        }
        public AddForm(AbstractMusic track) : this()
        {
            if (track != null)
            {
                comboBox1.SelectedItem = track.GetMusicType();
                textBox1.Text = track.Name;
                textBox2.Text = track.Artist;
                textBox3.Text = track.Length.ToString();
            }
        }
        private void AddForm_Load(object sender, EventArgs e)
        {
            
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MusicFactory factory = new MusicFactory();
            object type = comboBox1.SelectedItem;
            string name = textBox1.Text;
            string artist = textBox2.Text;
            int length = int.Parse(textBox3.Text);
            if (type!=null && name!="" && artist!="" && length > 0)
            {
                Music = factory.CreateTrack(comboBox1.SelectedItem.ToString(), textBox1.Text, textBox2.Text, length);
                button1.DialogResult = DialogResult.OK;
                _result = DialogResult.OK;
                Close();
            } else
            {
                label5.Text = "";
                label5.Text = "Not all fields are filled";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.DialogResult = DialogResult.Cancel;
            _result = DialogResult.Cancel;
            Close();
        }
    }
}
