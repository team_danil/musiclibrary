﻿using MusicDesktop;
using MusicLib;
using MusicLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5
{
    public partial class MainForm : Form
    {
        private AudioLib _lib;

        public AudioLib Lib
        {
            get { return _lib; }
            set { _lib = value; }
        }
        public MainForm()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            MusicFactory factory = new MusicFactory();
            Lib = factory.CreateMusic();
            LibUpdate();
        }

        private void LibUpdate()
        {
            listBox1.Items.Clear();
            foreach (AbstractMusic track in Lib.MusicList)
            {
                listBox1.Items.Add("[" + track.GetMusicType() + "] " + track.Artist + " - " + track.Name + " (" + track.Length + "s.)");
            }
            MusicCalculator calculator = new MusicCalculator();
            int length = calculator.GetTotalLength(Lib);
            label1.Text = "Music album length: " + length + "s. ";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            AddForm f2;
            f2 = new AddForm();
            f2.Text = "Add Track";
            f2.ShowDialog();
            if (f2.Result == DialogResult.OK)
            {
                MusicFactory factory = new MusicFactory();
                AbstractMusic track = factory.CreateTrack(f2.Music.GetMusicType(), f2.Music.Name, f2.Music.Artist, f2.Music.Length);
                Lib.AddToLib(track);
                LibUpdate();
            } else
            {
                LibUpdate();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                Lib.MusicList.RemoveAt(listBox1.SelectedIndex);
                LibUpdate();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                AddForm f2;
                f2 = new AddForm(Lib.MusicList[listBox1.SelectedIndex]);
                f2.Text = "Edit Track";
                f2.ShowDialog();
                if (f2.Result == DialogResult.OK)
                {
                    MusicFactory factory = new MusicFactory();
                    AbstractMusic track = factory.CreateTrack(f2.Music.GetMusicType(), f2.Music.Name, f2.Music.Artist, f2.Music.Length);
                    Lib.MusicList[listBox1.SelectedIndex] = track;
                    LibUpdate();
                }
                else
                {
                    LibUpdate();
                }
            }
        }
    }
}
