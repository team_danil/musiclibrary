﻿using MusicLib;
using MusicLogic;
using System;

namespace MusicConsole
{
    class MusicMain
    {
        static void Main(string[] args)
        {
            MusicFactory factory = new MusicFactory();
            AudioLib lib = factory.CreateMusic();
            MusicPrinter printer = new MusicPrinter();
            printer.PrintList(lib);
        }
    }
}
