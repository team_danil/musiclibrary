﻿using MusicLib;
using System;

namespace MusicLogic
{
    public class MusicCalculator
    {
        public int GetTotalLength(AudioLib lib) {
            int len = 0;
            foreach (AbstractMusic track in lib.MusicList) {
                len += track.Length;
            }
            return len;
        }
    }

    public class MusicFactory
    {
        public AbstractMusic CreateTrack(string type, string name, string artist, int length)
        {
            AbstractMusic track = type switch
            {
                "Instrumental" => new Instrumental(),
                "EDM" => new EDM(),
                "Dubstep" => new Dubstep(),
                "Vocaloid" => new Vocaloid(),
                _ => new Instrumental(),
            };
            track.Name = name;
            track.Artist = artist;
            track.Length = length;
            return track;
        }
        public AudioLib CreateMusic()
        {
            Dubstep dubstep = new Dubstep
            {
                Artist = "Garbox",
                Name = "Stone and pepper",
                Volume = 100.6,
                Length = 240
            };

            Vocaloid vocaloid = new Vocaloid
            {
                Artist = "Kanaria",
                Name = "MIRA",
                Length = 136,
                Program = "Vocaloid"
            };

            Vocaloid vocaloid2 = new Vocaloid
            {
                Artist = "GHOST",
                Name = "Appetite of People-Pleaser",
                Length = 323,
                Program = "v4Flower"
            };

            Instrumental instrumental = new Instrumental
            {
                Artist = "Re-Logic",
                Name = "Alternate Day",
                Length = 101,
                Instrument_type = "Piano"
            };

            AudioLib lib = new AudioLib();
            lib.AddToLib(dubstep);
            lib.AddToLib(vocaloid);
            lib.AddToLib(instrumental);
            lib.AddToLib(vocaloid2);

            return lib;
        }
    }

    public class MusicPrinter
    {
        public void PrintTrack(AbstractMusic track)
        {
            Console.Write(track.Artist + " - " + track.Name + " (" + track.Length + ")");
        }

        public void PrintList(AudioLib lib)
        {
            MusicCalculator calculator = new MusicCalculator();
            int length = calculator.GetTotalLength(lib);
            Console.Write("Треки в альбоме: \n");
            foreach (AbstractMusic track in lib.MusicList)
            {
                Console.Write("\t");
                PrintTrack(track);
                Console.Write("\n");
            }
            Console.Write("\nСуммарная длительность треков: " + length + "\n");
        }
    }
}
