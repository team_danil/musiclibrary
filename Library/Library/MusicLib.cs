﻿using System;
using System.Collections.Generic;

namespace MusicLib
{
    public abstract class AbstractMusic
    {
        private string _artist;
        private string _name;
        private int _length;
        public abstract string GetMusicType();
        public string Artist
        {
            get { return _artist; }
            set { _artist = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }
    }

    public class Instrumental : AbstractMusic
    {
        private string _instrumentType;
        private bool _hasVocal;
        public override string GetMusicType()
        {
            return "Instrumental";
        }
        public string Instrument_type
            {
                get { return _instrumentType; }
                set { _instrumentType = value; }
            }

        public bool Has_vocal
        {
            get { return _hasVocal; }
            set { _hasVocal = value; }
        }
    }

    public class EDM : AbstractMusic
    {
        private int _beatsPerMinute;
        public override string GetMusicType()
        {
            return "EDM";
        }
        public int BeatsPerMinute
        {
            get { return _beatsPerMinute; }
            set { _beatsPerMinute = value; }
        }
    }

    public class Dubstep : EDM
    {
        private double _volume;
        public override string GetMusicType()
        {
            return "EDM";
        }
        public double Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }
    }

    public class Vocaloid : AbstractMusic
    {
        private string _program;
        public override string GetMusicType()
        {
            return "Vocaloid";
        }
        public string Program
        {
            get { return _program; }
            set { _program = value; }
        }
    }

    public class AudioLib
    {
        private List<AbstractMusic> _musicList = new List<AbstractMusic>();

        public List<AbstractMusic> MusicList
        {
            get { return _musicList; }
            set { _musicList = value; }
        }

        public void AddToLib(AbstractMusic track)
        {
            _musicList.Add(track);
        }
    }
}
